#include "ParserUtils.h"

namespace parser{

std::vector<std::string> splitStr(const std::string& line) {
    std::vector<std::string> words;

    std::string cStr;
    for (const auto& c : line) {
        if (c == ' ') {
            if (!cStr.empty()) {
                words.emplace_back(std::move(cStr));
                cStr.clear();
            }
            continue;
        }

        cStr.push_back(c);
    }

    //* Add last word too
    if (!cStr.empty()) words.emplace_back(std::move(cStr));

    return words;
}

std::string printSet(const std::set<std::string>& st) {
    std::string res = "{ ";
    for (const auto& s : st) {
        res.append(s + " ");
    }
    res.append("}");
    return res;
}

std::string printMap(const std::map<std::string, std::string>& in) {
    std::string printed = "{ ";
    for (const auto& p : in) {
        printed.append("[ " + p.first + " -> " + p.second + " ] ");
    }
    printed.append("}");

    return printed;
}

std::string printVector(const std::vector<std::string>& vc) {
    std::string res = "{ ";
    for (const auto& s : vc) {
        res.append(s + " ");
    }
    res.append("}");
    return res;
}

std::string printRule(const Rule_t& r) {
    // { A : 
    std::string res = "{ " + r.first + " : ";
    for (const auto& rval : r.second) {
        res.append(rval + " ");
    }
    res += "}";

    return res;
}

std::string printRuleVector(const std::set<Rule_t>& rv) {
    std::string res = "[ ";
    for (const auto& rule : rv) {
        res.append(printRule(rule) + " ");
    }
    res += "]";

    return res;
}

} /* namespace parser */