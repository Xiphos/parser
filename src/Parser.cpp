#include "Parser.h"
#include <boost/multi_array.hpp>
#include <glog/logging.h>
#include <glog/log_severity.h>
namespace parser{

using std::string;


//  TODO: Refactor this to use common functions
ParserStatus Parser::InputGrammar(std::istream& in) {
    std::set<string> vars;
    std::set<string> terms;
    std::set<Rule_t> rules;
    string strt;

    string line;
    int count = 0;
    while (std::getline(in, line)) {
        if (line == "done") {
            break;
        }
        ++count;

        switch (count) {
            case 1: // First line: variables
                vars = getValues(line);
                if (vars.empty()) {
                    LOG(ERROR) << "Could not get variables from line " 
                        << count << '\n';
                }
                break;
            case 2: // Second line: terminal characters
                terms = getValues(line);
                if (terms.empty()) {
                    LOG(ERROR) << "Could not get variables from line "
                        << count << '\n';
                }
                for (const auto& term : terms) {
                    auto it = vars.find(term);
                    if (it != vars.end()) {
                        LOG(ERROR) << "\'" << *it << "\'"
                            << " repeated in terminals and variables\n";
                        return ParserStatus::INVALIDGRAMMAR;
                    }
                }
                break;
            case 3:
            default: // Input rules
            {
                auto rule = parseRule(line);

                // Add new transitions
                if (!rule.first.empty() && !rule.second.empty()) {
                    rules.insert(rule);
                }
                break;
            }
        }
    }

    // Check if variables/terminals are valid (already guaranteed to have no intersection)
    // TODO: refactor these to one function (if max length increases)
    for (const auto& var : vars) {
        if (var.size() > MAX_VARIABLE_LENGTH) {
            LOG(ERROR) << "Variable \"" << var << "\" is longer than maximum variable length "
                << MAX_VARIABLE_LENGTH << '\n';
            return ParserStatus::INVALIDGRAMMAR;
        }

        // >> Set start variable if it wasnt set (will be the first alphabetically)
        if (strt.empty()) {
            strt = var;
        }
    }

    // Enforce symbol length
    for (const auto& term : terms) {
        if (term.size() > MAX_TERMINAL_LENGTH) {
            LOG(ERROR) << "Terminal \"" << term << "\" is longer than maximum terminal length "
                << MAX_TERMINAL_LENGTH << '\n';

            return ParserStatus::INVALIDGRAMMAR;
        }
    }

    // Check if rules are valid
    for (const auto& rule : rules) {

        // >> Check that LSides are valid (must be variables)
        const auto& Lit = vars.find(rule.first);
        if (Lit == vars.end()) {
            LOG(ERROR) << "lvalue referenced a variable that wasn't declared: \""
                << rule.first << "\"\n";
            
            return ParserStatus::INVALIDGRAMMAR;
        }

        // >> Check that RSides are valid
        const auto& rvals = rule.second;
        for (const auto& rval : rvals) {

            // Might be a variable
            const auto& Rit = vars.find(rval);
            if (Rit == vars.end()) {
                // Might be a terminal
                const auto& tRit = terms.find(rval);

                if (tRit == terms.end()) {
                    LOG(ERROR) << "rvalue reference a variable/terminal that "
                        << "wasn't declared: \"" << rval << "\"\n";
                    
                    // It was neither
                    return ParserStatus::INVALIDGRAMMAR;
                }
            }
        }

        // >> Check that the rule is CNF:
            // A -> BC
            // A -> a
            // [Not implemented] A -> 0 (empty string)
        // TODO: Make something that converts the grammar to CNF?
        // A -> BC
        if (rule.second.size() == 2) {
            // TODO: does CNF require that neither is the start symbol?
            bool isValid = (vars.find(rule.second[0]) != vars.end()) &&
                (vars.find(rule.second[1]) != vars.end());
            
            if (!isValid) {
                LOG(ERROR) << "Rule " << printRule(rule) << " was invalid:"
                    << " Not in CNF (rval was not two variables)\n";
                return ParserStatus::INVALIDGRAMMAR;
            }
        } else if (rule.second.size() == 1) { // A -> a
            bool isValid = (terms.find(rule.second[0]) != terms.end());

            if (!isValid) {
                LOG(ERROR) << "Rule " << printRule(rule) << " was invalid:"
                    << " Not in CNF (rval was not a terminal)\n";
                return ParserStatus::INVALIDGRAMMAR;
            }
        } else {
            LOG(ERROR) << "Rule " << printRule(rule) << " was invalid:"
                << " Not in CNF (does not fit any CNF prototype)\n";
        }
    }

    _grammar = Grammar{vars, terms, rules, strt};

    if (_grammar.isValid()) {
        return ParserStatus::GRAMMARSUCCESS;
    } else {
        return ParserStatus::INVALIDGRAMMAR;
    }
}

std::set<std::string> Parser::getValues(const std::string& line) const {
    auto splitLine = splitStr(line);
    
    return std::set<std::string>(splitLine.begin(), splitLine.end());
}

Rule_t Parser::parseRule(const std::string& line) const {
    enum class State {
        INIT,
        VARTOKEN,
        RSIDEINIT,
        SPACE1,
        SPACE2,
        RSIDETOKEN,
        DONE
    };

    std::string variable;
    std::vector<std::string> rside;
    State s = State::INIT;

    // State Machine
    // TODO: make common logging function for errors
    for (const auto& c : line) {
        // Exit loop once done
        if (s == State::DONE) {
            break;
        }

        // State assignment
        switch (s) {
            case State::INIT:
                if (isalnum(c)) {
                    s = State::VARTOKEN;
                } else if (!isspace(c)) {
                    LOG(ERROR) << "Rule parsing error! got illegal character: "
                        << c << '\n';
                    rside = {};
                    variable = "";
                    s = State::DONE;
                }
                break;
            case State::VARTOKEN:
                if (isspace(c)) {
                    s = State::SPACE1;
                } else if (!isalnum(c)) {
                    LOG(ERROR) << "Rule parsing error! got illegal character: "
                        << c << '\n';
                    rside = {};
                    variable = "";
                    s = State::DONE;
                }
                break;
            case State::SPACE1:
                if (c == ':') {
                    s = State::RSIDEINIT;
                } else if (!isspace(c)) {
                    LOG(ERROR) << "Rule parsing error! expected production separator, got "
                        << c << '\n';
                    rside = {};
                    variable = "";
                    s = State::DONE;
                }
                break;
            case State::SPACE2:
                if (isalnum(c))
                    s = State::RSIDETOKEN;
                break;
            case State::RSIDEINIT:
                if (isalnum(c)) {
                    s = State::RSIDETOKEN;
                } else if (!isspace(c)) {
                    LOG(ERROR) << "Rule parsing error! invalid character"
                        << c << '\n';
                    rside = {};
                    variable = "";
                    s = State::DONE;
                }
                break;
            case State::RSIDETOKEN:
                if (isspace(c) || isblank(c))
                    s = State::SPACE2;
                break;
            default:
                LOG(FATAL) << "Fatal rule parsing error (invalid state)\n";
                rside = {};
                variable = "";
                s = State::DONE;
                break;
        }
        
        // State actions
        switch (s) {
            case State::INIT:
                break;
            case State::VARTOKEN:
                variable += c;
                break;
            case State::RSIDEINIT:
                break;
            case State::SPACE1:
                break;
            case State::SPACE2:
                break;
            case State::RSIDETOKEN:
                rside.push_back(std::string(1, c));
                break;
            case State::DONE:
                break;
            default:
                LOG(FATAL) << "Fatal rule parsing error (invalid state)\n";
                rside = {};
                variable = "";
                s = State::DONE;
                break;
        }
    }

    // Check that the rule is valid
    if (!variable.empty() && !rside.empty()) {
        if (variable.size() > MAX_VARIABLE_LENGTH) {
            LOG(ERROR) << "Invalid rule: expected max variable length of "
                << MAX_VARIABLE_LENGTH << ", got " << variable.size() << " characters\n";
            return {"", {}};
        }
        if (variable.size() > MAX_TERMINAL_LENGTH) {
            LOG(ERROR) << "Invalid rule: expected max variable length of "
                << MAX_VARIABLE_LENGTH << ", got " << variable.size() << " characters\n";
            return {"", {}};
        }
    }
    return {variable, rside};
}

// TODO: add CNFTYPE:: enum class (unit prod, prod)
ParserResult Parser::Parse(const std::string& str) {
    if (!_grammar.isValid()) {
        return ParserResult::UNINITIALIZED;
    }

    std::vector<std::string> sentence;
    std::transform(str.begin(), str.end(), std::back_inserter(sentence),
        [](const char c){
            std::string symbol;
            symbol.push_back(c);
            return symbol;
        });

    const auto& vars = _grammar.getVariables();
    const auto& terms = _grammar.getTerminals();
    const auto& rules = _grammar.getRules();

    int n = sentence.size();
    int r = vars.size();

    // Store the substr validity
    memo_t memo(boost::extents[n][n][r]);

    // Set the first row
    for (const auto& symbol : sentence) {
        auto sIt = terms.find(symbol);
        if (sIt == terms.end()) {
            LOG(ERROR) << "Invalid terminal \'" << symbol
                << "\' in sentence.\n";
            
            return ParserResult::INVALID;
        }

        auto s = std::distance(terms.begin(), sIt);

        // Find Unit productions that produces the current symbol
        for (const auto& rule : rules) {
            if (rule.second.size() != 1 || rule.second[0] != symbol) {
                continue;
            }
            auto vIt = vars.find(rule.first);

            // This variable can produce the symbol
            if (vIt != vars.end()) {
                auto v = std::distance(terms.begin(), vIt);

                // Set the matrix value
                memo[1][s][v] = true;
            } else {
                LOG(ERROR) << "Invalid Variable " << *vIt
                    << " found in rules\n";
                
                return ParserResult::INVALID;
            }
        }
    }

    for (memoIdx_t l = 2; l < n; ++l) {
        for (memoIdx_t s = 1; s < n-l+1; ++s) {
            for (memoIdx_t p = 1; p < l-1; ++p) {
                
                for (const auto& prod : rules) {
                    // Is CNF::R -> AB
                    if (prod.second.size() != 2) {
                        continue;
                    }

                    auto a = std::distance(vars.begin(),
                        vars.find(prod.first));
                    auto b = std::distance(vars.begin(), 
                        vars.find(prod.second[0]));
                    auto c = std::distance(vars.begin(),
                        vars.find(prod.second[1]));

                    if (memo[p][s][b] && memo[l-p][s+p][c]) {
                        memo[l][s][a] = true;
                    }
                }
            }
        }
    }

    if (memo[n][1][1])
        return ParserResult::VALID;
    else
        return ParserResult::INVALID;
}

}