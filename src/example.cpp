#include "Parser.h"
#include <glog/logging.h>

using parser::Parser;

int main(int argc, char *argv[]) {
    google::InitGoogleLogging(argv[0]);
    Parser parser;
    parser.InputGrammar();

    std::cout << "\n\n";
    if(!parser.GetGrammar().isValid()){
        std::cout << "Invalid grammar" << '\n';
        return -1;
    }
    std::cout << "Got grammar!" << '\n' <<
        parser.GetGrammar() << '\n';

    std::string sentence;

    std::cout << "Start variable? [Default: " << parser.GetGrammar().getStart()
        << "] (press enter to skip)" << '\n';
    std::string reqStart;
    std::cin >> reqStart;
    if (!reqStart.empty() && parser.GetGrammar().setStart(reqStart)) {
        std::cout << "Set start variable to " << reqStart
            << " successfuly." << '\n';
    } else {
        std::cout << "Using default start variable" << '\n';
    }

    std::cout << "Now input a sentence to parse:" << '\n';
    std::cin >> sentence;

    auto res = parser.Parse(sentence);

    if (res == parser::ParserResult::VALID) {
        std::cout << "Sentence was valid!" << '\n';
        return 0;
    } else {
        std::cout << "Sentence was invalid!" << '\n';
        return 1;
    }
}