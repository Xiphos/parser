#include "Grammar.h"
#include "ParserUtils.h"

namespace parser {

using std::string;

std::ostream& operator<< (std::ostream& os, const Grammar& g) {
    os << "Variables: " << printSet(g.getVariables()) << '\n'
        << "Terminals: " << printSet(g.getTerminals()) << '\n'
        << "Start Character: " << g.getStart() << '\n'
        << "Rules: " << printRuleVector(g.getRules()) << '\n';
    
    return os;
}

}