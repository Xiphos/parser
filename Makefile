CPP_COMPILER = g++
CPP_FLAGS = --std=c++17 -O3 -Wall
LIBSRC = src/Grammar.cpp\
		src/ParserUtils.cpp\
		src/Parser.cpp
EXAMPLE = src/example.cpp
TESTSRC = test/TestParserUtils.cpp test/TestParser.cpp test/TestBenchmarkUtils.cpp
BENCHMARKSRC = benchmark/BMParser.cpp benchmark/BMUtils.cpp
GTEST_LIBS=-lgtest_main  -lgtest -pthread
GBENCH_LIBS= -pthread -lbenchmark
LOGGING=-lglog -lgflags
INCLUDE = -Iinc/ -Ibenchmark/ -Ideps/
LIBS=-Ldeps/

all:
	@echo example tests benchmark lines

example: ${LIBSRC} ${EXAMPLE} lines
	${CPP_COMPILER} ${LIBSRC} ${LIBS} ${EXAMPLE} ${CPP_FLAGS}\
		-o bin/parser ${INCLUDE} ${LOGGING} -pthread

tests: ${LIBSRC} ${TESTSRC} lines
	${CPP_COMPILER} ${LIBSRC} ${LIBS} ${TESTSRC} benchmark/BMUtils.cpp ${CPP_FLAGS}\
		-o bin/tests ${INCLUDE} ${GTEST_LIBS} ${LOGGING}

benchmark: ${LIBSRC} ${BENCHMARKSRC} lines
	${CPP_COMPILER} ${LIBSRC} ${LIBS} ${BENCHMARKSRC} ${CPP_FLAGS}\
		-o bin/benchmark ${INCLUDE} ${GBENCH_LIBS} ${LOGGING}

lines:
	@find . -iregex '.*cpp$\' -or -iregex '.*h$\' -or -iname Makefile\
	 	-or -iname 'todo*' -or -iname 'readme.md' | grep -v deps | xargs wc -l > Lines.md

	@echo "Total lines written to Lines.md"