# Parser

## Intro

(eventually) a simple parser that inputs a grammar and parses sentences.  

``` make tests  ``` to build unit tests  
``` make example ``` to build with example  
``` make benchmark ``` to build benchmarking suite

Grammars are input in the following format:  
1. Variables
2. Terminal characters
3. Rules (any amount)
4. 'done'

Grammars must be CNF (for now). The CYK algorithm is used for parsing.

## Design

### Class Parser
- Contains a grammar that can be set using ``` InputGrammar() ```
- Once a grammar is set, a string can be parsed using [?]

### Class Grammar

## Testing
The GTest framework is used for unit testing. In the future, GMock will be used for more comprehensive tests. Files related to tests are put in the ``` test/ ``` directory, samples are in ``` TestGrammars/ ```. currently the tests must be run at the root directory to run tests that depend on those files.

## Benchmarking
The Google Benchmarking framework is used for benchmarking. Files related to benchmarks are put in the ``` benchmark/ ``` directory