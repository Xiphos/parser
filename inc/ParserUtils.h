#ifndef _SANDBOX_PARSERUTILS_H
#define _SANDBOX_PARSERUTILS_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include "ParserTypes.h"

namespace parser {

//* String utilities

/*
 * splitStr: splits the given line into a vector based on spaces
 */
std::vector<std::string> splitStr(const std::string& line);

/*
 * printSet: formats a set of strings: { %0 %1 %2 ... }
 */
std::string printSet(const std::set<std::string>& st);

/*
 * printMap: formats a map: { [%0a : %0b], [%1a : %1b], ... }
 */
std::string printMap(const std::map<std::string, std::string>& in);

/*
 * printVector: formats a vector of strings: { %0 %1 %2 ... }
 */
std::string printVector(const std::vector<std::string>& st);

/*
 * printRule: formats a Rule_t: {A -> B C D F ...}
 */
std::string printRule(const Rule_t& r);

/*
 * printRuleSet: formats a vector of Rule_t
 */
std::string printRuleVector(const std::set<Rule_t>& rv);
} /* namespace parser */

#endif