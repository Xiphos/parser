#ifndef _SANDBOX_PARSER_TYPES_H
#define _SANDBOX_PARSER_TYPES_H

#include <vector>
#include <string>
#include <boost/multi_array.hpp>

namespace parser{

using Rule_t = std::pair<std::string, std::vector<std::string>>;

// Error codes for parser
enum class ParserStatus {
    INVALIDGRAMMAR,
    GRAMMARSUCCESS,
};

// Parsing result for parser
enum class ParserResult {
    VALID,
    INVALID,
    UNINITIALIZED
};

typedef boost::multi_array<bool,3> memo_t;
typedef memo_t::index memoIdx_t;

static constexpr int MAX_VARIABLE_LENGTH = 1;
static constexpr int MAX_TERMINAL_LENGTH = 1;

}

#endif