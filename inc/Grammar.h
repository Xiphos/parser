#ifndef _SANDBOX_GRAMMAR_H
#define _SANDBOX_GRAMMAR_H

#include <iostream>
#include <set>
#include <string>
#include <map>

#include "ParserUtils.h"
#include "ParserTypes.h"

namespace parser{

class Grammar {
    public:
        Grammar(std::set<std::string> var, std::set<std::string> term, std::set<Rule_t> rules, std::string start) : 
            _variables(var), _terminals(term), _rules(rules), _start(start) {
                //! Must have all fields
                _isValid = !(var.empty()||term.empty()||rules.empty()||start.empty());
        }

        Grammar() {
            _isValid = false;
        }

        //* Setters

        inline bool setStart(const std::string& start) {
            if (_variables.find(start) != _variables.end()) {
                _start = start;
                return true;
            }
            return false;
        }

        //* Getters

        inline const std::set<std::string>& getVariables() const {return _variables;}
        inline const std::set<std::string>& getTerminals() const {return _terminals;}
        inline const std::set<Rule_t>& getRules() const {return _rules;}
        inline const std::string& getStart() const {return _start;}
        inline const bool isValid() const {return !(_variables.empty() ||
            _terminals.empty() || _rules.empty() || _start.empty());}

        //* Operators

        friend std::ostream& operator<<(std::ostream& out, const Grammar& g);

    private:
        std::set<std::string> _variables;
        std::set<std::string> _terminals;
        std::set<Rule_t> _rules;
        mutable std::string _start;

        bool _isValid;
};

} /* namespace parser */

#endif