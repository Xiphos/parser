#ifndef _SANDBOX_PARSER_H
#define _SANDBOX_PARSER_H

#include <algorithm>
#include <set>
#include <string>
#include <vector>

#include "Grammar.h"
#include "ParserTypes.h"

namespace parser {

class Parser {
private:
    Grammar _grammar;
protected:
    /*
     * parseRule: parses a grammar rule based on line
     * 
     * @param[in] line: the line to parse
     * @return a Rule_t corresponding to the line (empty on failure)
    */
    Rule_t parseRule(const std::string& line) const;

    /*
     * getValues: get a set of space separated values from line
     * 
     * @param[in] line: the line to get values from
     * @return a set of strings from line
    */
    std::set<std::string> getValues(const std::string& line) const;
public:
    Parser() = default;

    /*
    * InputGrammar(): input a grammar from the given istream
    * 
    * Format: (see https://en.wikipedia.org/wiki/Context-free_grammar)
    *      line 1: variables
    *      line 2: terminal symbols
    *      line 3 ... line (n-1): rules
    *              rule format: $VAR : $(VAR | TERM) *
    *      line n: done
    * the start symbol is the first variable given
    * 
    * @param[in] in: the input stream to read from (default cin)
    * @return ParserStatus indicating whether the operation was a success
    */
    ParserStatus InputGrammar(std::istream& in = std::cin);

    /*
     * GetGrammar()
     * 
     * @return a reference to the grammar this parser is using
     */
    inline Grammar& GetGrammar() {
        return _grammar;
    }

    /*
     * Initialized()
     * 
     * @return a bool indicating if the parser has a grammar to follow
     */
    inline bool Initialized() const {
        return _grammar.isValid();
    }

    /*
     * Parse() return a ParserResult indicating if the given sentence is properly formed
     * 
     * @param[in] str: a string representing the sentence to be parsed
     * @return ParserResult, returns UNINITIALIZED if there was no grammar given
     */
    ParserResult Parse(const std::string& str);

    ~Parser() = default;
};

}

#endif