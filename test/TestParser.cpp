#include <gtest/gtest.h>
#include "TestParser.h"

namespace parser{
/*
 * Test getValues() basic string
 */
TEST_F(TestParser, TestGetValuesBasic) {
    const std::string str = "AB CD  EEE F   GG";
    const int WORDS = 5u;

    auto out = getValues(str);

    EXPECT_EQ(out.size(), WORDS);

    for (const auto& s : out) {
        EXPECT_EQ(s.find_first_of("\t\n "), s.npos);
    }
}

/*
 * Test getValues() repeat values
 */
TEST_F(TestParser, TestGetValuesRepeatedValues) {
    const std::string str = "AB  CD E  CC     AB CD";
    const int UNIQ_WORDS = 4u;

    auto out = getValues(str);

    ASSERT_EQ(out.size(), UNIQ_WORDS);
}

/*
 * Test getValues() empty string
 */
TEST_F(TestParser, TestGetValuesEmptyString) {
    const std::string str = "";

    auto out = getValues(str);

    ASSERT_TRUE(out.empty());
}

/*
 * Test valid rule parsing
 */
// TODO: Move test ruleTests into this function instead of reloading the
//      grammar file every test
void TestParser::TestParseValidRule(const ruleTest_t& rtest) {

    auto parsed = parseRule(rtest.str);

    EXPECT_FALSE(parsed.first.empty());
    EXPECT_FALSE(parsed.second.empty());

    // TODO: make a common function for outputting the parsed rule
    EXPECT_EQ(parsed.first, rtest.lval)  << "Parsed rule: {"
        << parsed.first << ", " << printVector(parsed.second) << "}\n"
        << "Expected: {" << rtest.lval << ", " << printVector(rtest.rval) << "}";
    EXPECT_EQ(parsed.second, rtest.rval) << "Parsed rule: {"
        << parsed.first << ", " << printVector(parsed.second) << "}\n"
        << "Expected: {" << rtest.lval << ", " << printVector(rtest.rval) << "}";
}

/*
 * Test parseRule() basic rules 0
 */
TEST_F(TestParser, TestParseRuleBasic0) {
    TestParseValidRule({
        "A :  B  ",
        "A",
        {"B"}
    });
}

/*
 * Test parseRule() basic rules 1
 */
TEST_F(TestParser, TestParseRuleBasic1) {
    TestParseValidRule({
        "   A   :  C",
        "A",
        {"C"}
    });
}

/*
 * Test parseRule() basic rules 2
 */
TEST_F(TestParser, TestParseRuleBasic2) {
    TestParseValidRule({
        "A   :  4  ",
        "A",
        {"4"}
    });
}

/*
 * Test parseRule() basic rules 3
 */
TEST_F(TestParser, TestParseRuleBasic3) {
    TestParseValidRule({
        "4 : 3",
        "4",
        {"3"}
    });
}

/*
 * Test parseRule() basic rules 4
 */
TEST_F(TestParser, TestParseRuleBasic4) {
    TestParseValidRule({
        "A :B",
        "A",
        {"B"}
    });
}

/*
 * Test parseRule() basic rules 5
 */
TEST_F(TestParser, TestParseRuleBasic5) {
    TestParseValidRule({
        "A : B C",
        "A",
        {"B", "C"}
    });
}

// TODO: move test strings into this function
void TestParser::TestParseInvalidRule(const std::string& str) {
    auto parsed = parseRule(str);

    // TODO: make a common function for outputting the parsed rule
    EXPECT_TRUE(parsed.first.empty())  << "Parsed rule: {"
        << parsed.first << ", " << printVector(parsed.second) << "}"
        << " expected invalid (empty)";
    EXPECT_TRUE(parsed.second.empty()) << "Parsed rule: {"
        << parsed.first << ", " << printVector(parsed.second) << "}"
        << " expected invalid (empty)";
}

/*
 * Test parseRule() invalid format: no colon
 */
TEST_F(TestParser, TestParseRuleInvalid_NoColon) {
    TestParseInvalidRule("A    B");
}

/*
 * Test parseRule() invalid format: invalid token name
 */
TEST_F(TestParser, TestParseRuleInvalid_InvalidTokenName) {
    TestParseInvalidRule("$##( : (#((R");
}

/*
 * Test parseRule() invalid format: multiple lside tokens
 */
TEST_F(TestParser, TestParseRuleInvalid_MultipleLTokens) {
    TestParseInvalidRule("A B : C");
}



/*
 * Test parseRule() invalid format: invalid token length
 */
TEST_F(TestParser, TestParseRuleInvalid_InvalidTokenLength) {
    std::string lToken;
    std::string rToken;
    for (int i = 0; i < (MAX_VARIABLE_LENGTH + 5); ++i) {
        lToken.push_back('A');
    }
    for (int i = 0; i < (MAX_TERMINAL_LENGTH + 5); ++i) {
        rToken.push_back('B');
    }
    std::string str = lToken + " : " + rToken;
    TestParseInvalidRule(str);
}


// Load a test grammar from TestGrammars directory into &stream
void loadTestGrammar(uint id, std::ifstream& stream) {
    ASSERT_LE(id, MAX_TESTGRAMMAR_ID) << "Invalid test grammar ID";

    auto str = TEST_GRAMMARS_DIR + "/" + std::to_string(id) + TEST_GRAMMARS_EXT;
    stream = std::ifstream(str);
}

/*
 * TestGrammar 0
 */
TEST_F(TestParser, TestInputGrammar_TestValidGrammar0) {
    std::ifstream stream;

    // Fail if cant load grammar
    ASSERT_NO_FATAL_FAILURE(loadTestGrammar(0u, stream));

    ASSERT_EQ(InputGrammar(stream), ParserStatus::GRAMMARSUCCESS);
    EXPECT_TRUE(GetGrammar().isValid());

    // TODO: replace these with constants per test grammar
    EXPECT_EQ(GetGrammar().getRules().size(), 5u);
    EXPECT_EQ(GetGrammar().getStart(), *GetGrammar().getVariables().begin());
    EXPECT_EQ(GetGrammar().getTerminals().size(), 2u);
    EXPECT_EQ(GetGrammar().getVariables().size(), 2u);
}

/*
 * TestGrammar 1
 */
TEST_F(TestParser, TestInputGrammar_TestValidGrammar1) {
    std::ifstream stream;

    // Fail if cant load grammar
    ASSERT_NO_FATAL_FAILURE(loadTestGrammar(1u, stream));

    ASSERT_EQ(InputGrammar(stream), ParserStatus::GRAMMARSUCCESS);
    EXPECT_TRUE(GetGrammar().isValid());

    // TODO: replace these with constants per test grammar
    EXPECT_EQ(GetGrammar().getRules().size(), 5u);
    EXPECT_EQ(GetGrammar().getStart(), *GetGrammar().getVariables().begin());
    EXPECT_EQ(GetGrammar().getTerminals().size(), 4u);
    EXPECT_EQ(GetGrammar().getVariables().size(), 3u);
}

}
