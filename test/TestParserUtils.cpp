#include <gtest/gtest.h>
#include "ParserUtils.h"

namespace parser{

/*
 * Test basic string splitting
 */
TEST(TestParserUtils, TestSplitBasic) {
    const std::string str = "A B C  D   E F    G  H";
    const int WORDS = 8u;
    const int WORDSIZE = 1u;

    auto split = splitStr(str);

    EXPECT_FALSE(split.empty());
    EXPECT_EQ(split.size(), WORDS);
    for (const auto& w : split) {
        EXPECT_EQ(w.size(), WORDSIZE);
    }
}

/*
 * Test splitting an empty string
 */
TEST(TestParserUtils, TestSplitEmptyString) {
    const std::string empty = "";

    auto split = splitStr(empty);

    ASSERT_TRUE(split.empty());
}

} /* namespace parser */