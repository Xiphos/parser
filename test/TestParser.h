#ifndef _SANDBOX_TEST_PARSER_H
#define _SANDBOX_TEST_PARSER_H

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include "Parser.h"

namespace parser{

static constexpr uint MAX_TESTGRAMMAR_ID = 1;
static const std::string TEST_GRAMMARS_DIR = "TestGrammars";
static const std::string TEST_GRAMMARS_EXT = ".txt";

struct ruleTest_t {
    const std::string str;
    const std::string lval;
    const std::vector<std::string> rval;
};

class TestParser : public ::testing::Test, public Parser {
protected:
    void TestParseValidRule(const ruleTest_t& rtest);
    void TestParseInvalidRule(const std::string& str);

};

}
#endif