#include <gtest/gtest.h>
#include "BMUtils.h"

// Large number to reduce collisions
static constexpr size_t NUM = 65536u;

/*
 * BASIC FUNCTIONALITY TESTS
 */
TEST(TestBenchmarkUtils, RandomNumber) {
    // Produces random numbers (should fail very rarely)
    //  Probability (1/NUM)
    auto res1 = benchmarkUtils::RandomNumber(0, NUM);
    auto res2 = benchmarkUtils::RandomNumber(0, NUM);

    ASSERT_NE(res1, res2);
}

TEST(TestBenchmarkUtils, RandomSentence) {
    // Produces different sentences (should fail very rarely)
    //  Probability 1 / (64 * 64)
    auto res1 = benchmarkUtils::RandomSentence(64, 64);
    auto res2 = benchmarkUtils::RandomSentence(64, 64);

    ASSERT_NE(res1, res2);
}

TEST(TestBenchmarkUtils, RandomString) {
    // Produces different strings (should fail very rarely)
    //  Probability 
    auto res1 = benchmarkUtils::RandomString(NUM);
    auto res2 = benchmarkUtils::RandomString(NUM);

    ASSERT_NE(res1, res2);
}

/*
 * EDGE CASE TESTS
 */

TEST(TestBenchmarkUtils, RandomNumberInvalidArgs) {
    // Invalid arguments: min > max
    auto res = benchmarkUtils::RandomNumber(SIZE_MAX, 0);
    ASSERT_EQ(res, -1);
}

TEST(TestBenchmarkUtils, RandomNumberSameValue) {

    // Predictable result when min = max
    auto res = benchmarkUtils::RandomNumber(NUM, NUM);

    ASSERT_EQ(res, NUM);
}

TEST(TestBenchmarkUtils, RandomSentenceZeroArg) {
    // Invalid args should result in empty string

    auto res = benchmarkUtils::RandomSentence(0, NUM);
    EXPECT_TRUE(res.empty()) << "Zero number of words";

    res = benchmarkUtils::RandomSentence(NUM, 0);
    EXPECT_TRUE(res.empty()) << "Zero maxWordLength";

    res = benchmarkUtils::RandomSentence(0, 0);
    EXPECT_TRUE(res.empty()) << "Zero number of words and maxWordLength";
}

TEST(TestBenchmarkUtils, RandomStringZeroArg) {
    // Invalid args should result in empty string

    auto res = benchmarkUtils::RandomString(0);
    ASSERT_TRUE(res.empty());
}