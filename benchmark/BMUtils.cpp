#include "BMUtils.h"

namespace benchmarkUtils {

std::string RandomString(size_t size) {
    if (size == 0) return "";

    // Symbols that can comprise the string
    static constexpr size_t TABLE_SZ = 64;
    static constexpr char table[TABLE_SZ] = {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
        'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
        'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F',
        'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
        'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
        'W', 'X', 'Y', 'Z', '0', '1', '2', '3',
        '4', '5', '6', '7', '8', '9', '+', '=',
    };

    std::string randString;
    randString.reserve(size);

    // Add [size] random chars
    while (size-- > 0) {
        auto rIndex = RandomNumber(0, TABLE_SZ-1);
        randString.push_back(table[rIndex]);
    }

    return randString;
}

size_t RandomNumber(size_t min, size_t max) {
    if (min > max) return -1;

    static std::random_device r;
    static std::default_random_engine rEngine(r());
    std::uniform_int_distribution<size_t> dist(min, max);

    return dist(rEngine);
}

std::string RandomSentence(size_t words, size_t maxWordLength) {
    std::string sentence;
    if (words == 0 || maxWordLength == 0) return sentence;

    while (words-- > 0) {
        auto word = RandomString(RandomNumber(1, maxWordLength));

        sentence += word + (words == 0?"":" ");
    }

    return sentence;
}

}