#include "BMParser.h"
#include "BMUtils.h"


// Benchmark GetValues() function
BENCHMARK_F(BenchmarkParser, GetValues)(benchmark::State& state){
    // Setup
    const std::string str = "AA BB  BB CC  CC DD";

    // Execute
    for (auto _ : state){
        getValues(str);
    }
}

// Benchmark ParseRule()
// TODO: Use the benchmark API to test different rule lengths
BENCHMARK_F(BenchmarkParser, ParseRule)(benchmark::State& state){
    // Setup
    const std::string rl = " A : a b ";

    // Execute
    for (auto _ : state){
        parseRule(rl);
    }
}

// Benchmarks for ParserUtils
static void BMSplitStr(benchmark::State& state) {
    // Setup
    auto words = state.range(0);
    auto testStr = benchmarkUtils::RandomSentence(words, 20);

    // Execute
    for (auto _ : state) {
        parser::splitStr(testStr);
    }
} BENCHMARK(BMSplitStr)->Range(2,1024)->RangeMultiplier(2);

/* Benchmark Benchmark Utilities */
static void BMRandomNumber(benchmark::State& state) {
    // Setup
    auto min = 0;
    auto max = SIZE_MAX;

    // Execute
    for (auto _ : state) {
        benchmarkUtils::RandomNumber(min,max);
    }
} BENCHMARK(BMRandomNumber);

static void BMRandomString(benchmark::State& state) {
    // Setup
    auto length = state.range(0);

    // Execute
    for (auto _ : state) {
        benchmarkUtils::RandomString(length);
    }
} BENCHMARK(BMRandomString)->Range(2,4096)->RangeMultiplier(2);

static void BMRandomSentence(benchmark::State& state) {
    // Setup
    auto words = state.range(0);
    auto maxWordSz = 128ul;

    // Execute
    for (auto _ : state) {
        benchmarkUtils::RandomSentence(words, maxWordSz);
    }
} BENCHMARK(BMRandomSentence)->Range(2,4096)->RangeMultiplier(2);



BENCHMARK_MAIN();