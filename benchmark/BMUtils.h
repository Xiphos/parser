#ifndef _SANDBOX_BENCHMARK_UTILS_H
#define _SANDBOX_BENCHMARK_UTILS_H

#include <string>
#include <random>

namespace benchmarkUtils {

/* RandomString: generate a random string with base64 characters
 *
 * @param[in] size: the length of the resulting string
 * @return a random string of length [size]
 */
std::string RandomString(size_t size);

/* RandomNum: generate a random number on the interval [min, max]
 *
 * @param[in] min: the minimum value to return
 * @param[in] max: the maximum value to return
 * @return a random number on the interval [min, max], -1 if error
 */
size_t RandomNumber(size_t min, size_t max);

/* RandomSentence: generate a random space separated string of words
 *
 * @param[in] words: number of words in the sentence
 * @param[in] maxWordLength: the maximum length of a word
 * @return a space separated string of words (empty on error)
 */
std::string RandomSentence(size_t words, size_t maxWordLength);

}

#endif